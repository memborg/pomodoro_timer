use std::time::Duration;
use std::thread::sleep;

fn main() {
    let mut seconds = 0;
    let mut minutes = 0;

    loop {
        clear();
        println!("Pomodoro {}:{}", minutes, seconds);

        if seconds == 60 {
            seconds = 0;
            minutes = minutes + 1;
        }

        if minutes == 25 {
            println!("");
            println!("Take a break!");
            break;
        }

        sleep(Duration::from_millis(1000));

        seconds = seconds + 1;
    }
}

fn clear() {
    // Clears the entire terminal
    print!("{}[2J", 27 as char);
}
